import numpy as np
import tensorflow as tf
from tensorflow.contrib.learn.python.learn.estimators import model_fn as model_fn_lib
from tensorflow.contrib.learn.python.learn.estimators import estimator


class ConvRBM(object):
    def __init__(self,
                 n_visible,
                 n_hidden,
                 filter_shape,
                 pooling_factor=2
                 ):

        self.W = tf.Variable(tf.truncated_normal([filter_shape, filter_shape, 1, n_hidden], stddev=0.1))
        self.b_h = tf.Variable(tf.zeros([1, n_hidden]))
        self.b_v = tf.Variable(0)

        self.n_visible = n_visible
        self.n_hidden = n_hidden
        self.pooling_factor = pooling_factor
        self.filter_shape = filter_shape

    def __convolve(self, x, W):
        return tf.nn.conv2d(x, W, strides=[1, self.filter_shape, self.filter_shape, 1], padding='SAME')

    @staticmethod
    def __invert_convolution(W):
        return tf.reverse(W, axis=[0, 1])

    def __pool_groups(self, groups):
        filter = tf.constant(tf.ones([self.pooling_factor, self.pooling_factor, self.n_hidden, self.n_hidden / self.pooling_factor]))
        return tf.nn.conv2d(groups, filter, strides=[1, self.pooling_factor, self.pooling_factor, 1], padding='SAME')

    def activate_hidden(self, s_v):
        information = self.__convolve(s_v, self.__invert_convolution(self.W)) + self.b_h
        e_inf = tf.exp(information)
        return e_inf / self.__pool_groups(e_inf)

    def activate_pool(self, s_v):
        information = self.__convolve(s_v, self.__invert_convolution(self.W)) + self.b_h
        e_inf = tf.exp(information)
        return 1. - 1. / self.__pool_groups(e_inf)

    def p_given_v(self, s_v):
        E_p = self.activate_pool(s_v)
        s_p = tf.contrib.distributions.Bernoulli(probs=E_p, dtype=tf.float32).sample()
        return s_p, E_p

    def h_given_v(self, s_v):
        E_h = self.activate_hidden(s_v)
        s_h = tf.contrib.distributions.Bernoulli(probs=E_h, dtype=tf.float32).sample()
        return s_h, E_h

    def activate_visible(self, s_h):
        E = tf.reduce_sum(self.__convolve(s_h, self.W) + self.b_v, axis=3)

        if self.visible_type == 'binary':
            return tf.nn.sigmoid(E)
        elif self.visible_type == 'real':
            return E

    def v_given_h(self, s_h):
        E_v = self.activate_visible(s_h)

        if self.visible_type == 'binary':
            s_v = tf.contrib.distributions.Bernoulli(probs=E_v, dtype=tf.float32).sample()
        elif self.visible_type == 'real':
            s_v = tf.truncated_normal(tf.shape(E_v), E_v)

        return s_v, E_v

    def gibbs_hvh(self, s_h0):
        s_v1, E_v1 = self.v_given_h(s_h0)
        s_h1, E_h1 = self.h_given_v(s_v1)
        return s_v1, E_v1, s_h1, E_h1

    def gibbs_vhv(self, s_v0):
        s_h1, E_h1 = self.h_given_v(s_v0)
        s_v1, E_v1 = self.v_given_h(s_h1)
        return s_h1, E_h1, s_v1, E_v1

    def free_energy(self, s_v):
        with tf.name_scope('free_energy'):
            interaction = self.__convolve(s_v, self.W) + self.b_h
            hidden_term = tf.reduce_sum(
                tf.nn.softplus(interaction),
                axis=[1, 2, 3]
            )

            if self.visible_type == 'binary':
                vbias_term = tf.reduce_sum(s_v, axis=[1, 2]) * self.b_v
            elif self.visible_type == 'real':
                vbias_term = (tf.reduce_sum(s_v, axis=[1, 2]) - self.b_v)**2 / 2.

            return vbias_term - hidden_term

    def constrastive_divergence_loss(self, x, k=15, persistent_state=None):
        with tf.name_scope('constrastive_divergence_loss'):
            if persistent_state is not None:
                s_h0, E_h0 = (persistent_state, None)
            else:
                s_h0, E_h0 = self.h_given_v(x)

            s_vk, E_vk, s_hk, E_hk = (None, None, s_h0, E_h0)
            for _ in range(k):
                s_vk, E_vk, s_hk, E_hk = self.gibbs_hvh(s_hk)

            persistent_state.assign(s_hk)
            s_vk = tf.stop_gradient(s_vk)
            return tf.reduce_mean(self.free_energy(x)) - tf.reduce_mean(self.free_energy(s_vk))

    def fit(self, x, learning_rate, target_sparsity=None, optimizer=None, k=15, persistent_state=None, global_step=None):
        if optimizer is None:
            optimizer = tf.train.GradientDescentOptimizer(learning_rate)

        if target_sparsity is not None:
            N_h = self.n_visible / self.filter_shape
            db_h = tf.reduce_mean(target_sparsity - 1. / N_h**2 * tf.reduce_sum(self.activate_hidden(x), axis=[1, 2]))
            self.b_h = self.b_h - db_h

        loss = self.constrastive_divergence_loss(x, k, persistent_state)
        train_step = optimizer.minimize(loss, global_step=global_step, var_list=[self.W, self.b_v, self.b_h])
        return train_step, loss

    def encode(self, x):
        return self.p_given_v(x)

    def generate(self, p, chain_length=1000):
        s_vk = x
        for _ in range(chain_length):
            s_hk, E_hk, s_vk, E_vk = self.gibbs_vhv(s_vk)
        return s_vk


