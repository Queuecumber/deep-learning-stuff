import tensorflow as tf
from tensorflow.contrib.learn.python.learn.estimators import model_fn as model_fn_lib
from tensorflow.contrib.learn.python.learn.estimators import estimator


class RandomForest(object):
    def __init__(self, depth, n_trees, split_type):
        self.depth = depth
        self.n_trees = n_trees
        self.split_type = split_type

    @staticmethod
    def entropy(y):
        p = tf.reduce_sum(y, axis=0) / tf.shape(y)[0]
        return - tf.reduce_sum(p * tf.log(p))

    def information_gain(self, left_set, right_set):
        full_set = tf.stack([left_set, right_set])
        full_set_entropy = self.entropy(full_set)

        left_entropy_ratio = tf.shape(left_set)[0] / tf.shape(full_set)[0] * self.entropy(left_set)
        right_entropy_ratio = tf.shape(right_set)[0] / tf.shape(full_set)[0] * self.entropy(right_set)

        return full_set_entropy - (left_entropy_ratio + right_entropy_ratio)

    def sample_thresholds(self, responses):
        pass

    def apply_axis(self, x, y):
        dim = tf.shape(x)[1]
        augmented = tf.concat([x, tf.ones([tf.shape(x)[0], 1])], axis=1)

        random_axes = tf.one_hot(tf.random_uniform([self.n_features], minval=0, maxval=dim, dtype=tf.int32))
        random_biases = tf.random_uniform([self.n_features, 1], minval=tf.reduce_min(x), maxval=tf.reduce_max(x))

        decision_boundaries = tf.concat([random_axes, random_biases], axis=1)
        responses = augmented @ tf.transpose(decision_boundaries)

        thresholds = self.sample_thresholds(responses)



