{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# TensorFlow Decision Trees\n",
    "\n",
    "Decision trees are simple classifiers that use a tree structure to reduce the entropy, a measure of the randomness, of a training set. Each node in the tree is given some subset of the training data. It tries to split the data into two parts such that each part has less entropy. It then gives these smaller sets to its child nodes for them to split. When the sets are small enough, or have a small enough difference in entropy, a node will instead attempt to compute a posterior distribution over the set of samples it was given, creating a leaf node of the tree. The finished tree can be used to classify novel samples by passing the sample from the root node to its leaves and then using the probability distribution to classify using maximum a posteriori classification. This document follows the process for training a single node in a decision tree using the Randomized Node Optimization algorithm popularized by Shotton et al. in his work on the original Kinect (https://www.microsoft.com/en-us/research/publication/real-time-human-pose-recognition-in-parts-from-a-single-depth-image/). See also his book (https://link.springer.com/book/10.1007%2F978-1-4471-4929-3) for a full treatment of the theory and a thorough literature review. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Imports\n",
    "Import TensorFlow and make an InteractiveSession to evaluate, we also seed the random number generator so that repeated runs of the notebook come out the same. Note that repeated runs of individual kernels could still change though."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# use tensorflow in interactive mode\n",
    "import tensorflow as tf\n",
    "sess = tf.InteractiveSession()\n",
    "tf.set_random_seed(42)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## 2. Set up some example data\n",
    "We want a simple dataset to test the algorithm so we can visualize. Pick simple numbers, 4 samples from $\\mathbb{R}^3$. We also want a mix of positive and negative numbers to make sure signs arent causing problems. Resulting dataset is a rank-2 tensor, $4 \\times 3$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[-1.,  2., -3.],\n",
       "       [ 4., -5.,  6.],\n",
       "       [ 7.,  8.,  9.],\n",
       "       [-2., -6., -4.]], dtype=float32)"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# sample data is a simple set of 4 samples in R^3, mix of + and -\n",
    "x = tf.constant([[-1, 2, -3], [4, -5, 6], [7, 8, 9], [-2, -6, -4]], dtype=tf.float32)\n",
    "\n",
    "# dim is the sample dimension (3)\n",
    "dim = tf.shape(x)[1]\n",
    "\n",
    "# n is the number of samples\n",
    "n = tf.shape(x)[0]\n",
    "\n",
    "# show the sample data\n",
    "x.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because we are going to be evaluating these samples with respect to surfaces in $\\mathbb{R}^3$, we want form homogenious samples in $\\mathbb{P}^3$ by augmenting them with a 1's vector. This allows us to compute surface response by simply multiplying $r_{i,j} = x_i \\cdot l_j$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[-1.,  2., -3.,  1.],\n",
       "       [ 4., -5.,  6.,  1.],\n",
       "       [ 7.,  8.,  9.,  1.],\n",
       "       [-2., -6., -4.,  1.]], dtype=float32)"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# we then convert to P^3 to make surface responses simpler to compute\n",
    "augmented = tf.concat([x, tf.ones([n, 1], dtype=tf.float32)], axis=1)\n",
    "augmented.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we make some labels for our data. We will deal with a two class problem. Note that we use one-hot labels"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 1.,  0.],\n",
       "       [ 1.,  0.],\n",
       "       [ 0.,  1.],\n",
       "       [ 0.,  1.]], dtype=float32)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "labels = tf.one_hot([0,0,1,1], depth=2)\n",
    "labels.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally we set up some constants for later, that we will compute 5 random surfaces and check 3 random thresholds for each surface"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# finally we will compute 5 surfaces and check 3 thesholds per surface \n",
    "n_surfaces = 5\n",
    "n_thresholds = 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Get random axis-aligned surfaces\n",
    "Next we compute random axis-aligned surfaces. We stick to axis-aligned surfaces to keep things simple. An axis-aligned surface in $\\mathbb{P}^n$ takes the form\n",
    "$$l = \\begin{bmatrix}{x_0 \\\\ x_1 \\\\ x_2 \\\\ \\vdots \\\\ x_n \\\\ b}\\end{bmatrix}$$\n",
    "$$x_i \\in \\{0, 1\\}, \\sum_{i=0}^{n}{x_i} = 1$$\n",
    "e.g. exactly one of the dimensions $x_i$ is 1, the rest are 0, with an arbitrary bias term $b$. We sample uniformly random values for the bias term from the interval $[\\min(x),\\max(x)]$ as a heuristic to generate relevant biases from the training set"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is a TensorFlow trick to get random axes quickly. We generate uniformly random numbers, one for each surface, in the interval $[0,d]$ where d is `dim`, defined above, the dimensionality of the sample space. We then generate one-hot vectors from those random numbers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "random_axes = tf.one_hot(tf.random_uniform([n_surfaces], minval=0, maxval=dim, dtype=tf.int32), depth=dim)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then sample the biases as described above and concatenate them to the one-hot vector to form the final surfaces"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 0.        ,  0.        ,  1.        ,  8.2982769 ],\n",
       "       [ 1.        ,  0.        ,  0.        , -0.47197437],\n",
       "       [ 1.        ,  0.        ,  0.        , -2.43092918],\n",
       "       [ 1.        ,  0.        ,  0.        ,  0.59355831],\n",
       "       [ 0.        ,  0.        ,  1.        ,  7.09166908]], dtype=float32)"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "random_biases = tf.random_uniform([n_surfaces, 1], minval=tf.reduce_min(x), maxval=tf.reduce_max(x))\n",
    "surfaces = tf.concat([random_axes, random_biases], axis=1)\n",
    "surfaces.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Compute responses to axis-aligned surfaces\n",
    "We can now compute each samples response to the surfaces. A line in $\\mathbb{P}^3$ is defined as $0 = l \\cdot x$. We define the response for a sample $x$ to a surface $l$ as $r = l \\cdot x$. If $r > 0$, then the sample is {above, to the right of, etc} the line defined by $l$. If $r < 0$, then the sample is {below, left of, whatever} the line $l$. Note that those directions lose all meaning in the general case, but these lines still divide the space the exist in and we use that division to route samples in a decision tree. Samples with a positive response are routed to the right child, samples with a negative response are routed to the left child (samples with 0 response lie on the surface and can be routed in either direction). "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we wrap the result of this in a constant so that random numbers are not redrawn during later steps"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[  5.56634331,   5.6865406 ,   4.46796989,   9.66289711,\n",
       "         -1.17514706],\n",
       "       [ -1.43365669,  10.6865406 ,  -2.53203011,   2.66289711,\n",
       "          7.82485294],\n",
       "       [ 11.56634331,  13.6865406 ,  10.46796989,  15.66289711,\n",
       "         10.82485294],\n",
       "       [ -2.43365669,   4.6865406 ,  -3.53203011,   1.66289711,\n",
       "         -2.17514706]], dtype=float32)"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "responses = tf.constant((augmented @ tf.transpose(surfaces)).eval())\n",
    "responses.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Sample thresholds for each response at even quantiles\n",
    "To further generalize the method described in Section (4) we introduce a threshold $\\tau$. In the previous section, we split based on $r > 0$, we now generalize that to $r > \\tau$ and propose values for $\\tau$ that should split the range of the training set. These proposals are based on quantiles of the responses. Since we cannot say for certain how the training data is distributed, it make sense to propose more thresholds in areas of higher density.\n",
    "\n",
    "Note that we use the simplest possible quantile estimation by, given a vector of responses $r \\in \\mathbb{R}^n$ dividing the interval $[\\min_{n}(r),\\max_{n}(r)]$ evenly by `n_thresholds`, a proper method should estimate `n_thresholds` quantiles from $r$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 11.56634331,  13.6865406 ,  10.46796989,  15.66289711,\n",
       "         10.82485294]], dtype=float32)"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "response_max = tf.reduce_max(responses, axis=0, keep_dims=True)\n",
    "response_max.eval()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[-2.43365669,  4.6865406 , -3.53203011,  1.66289711, -2.17514706]], dtype=float32)"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "response_min = tf.reduce_min(responses, axis=0, keep_dims=True)\n",
    "response_min.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to choose `n_thresholds` proposals for $\\tau$ we split the space between the min and max values evenly into `n_thresholds + 1` regions. This is a tensorflow trick for doing that based on linspace. Since we have 3 thesholds, we need 4 even partitions, which should be at `0.25, 0.50, 0.75` of the way between the min and max value. We can compute these breakpoints for a general `n_thresholds` by computing the first threshold, then stepping to the last threshold producing a total of n_thresholds values, which the linspace function does for us"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 0.25],\n",
       "       [ 0.5 ],\n",
       "       [ 0.75]], dtype=float32)"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "spacer = 1. / (n_thresholds + 1)\n",
    "threshold_spacing = tf.reshape(tf.linspace(start=spacer, stop=1. - spacer, num=n_thresholds), [-1, 1])\n",
    "threshold_spacing.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally we can scale these percentage-based breakpoints to match the range of values from the min and max computed above, the end result is a $3 \\times 5$ tensor, 3 thresholds for each of the 5 surfaces that we compputed responses for."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[  1.06634331,   6.9365406 ,  -0.03203011,   5.16289711,\n",
       "          1.07485294],\n",
       "       [  4.56634331,   9.1865406 ,   3.46796989,   8.66289711,\n",
       "          4.32485294],\n",
       "       [  8.06634331,  11.4365406 ,   6.96796989,  12.16289711,\n",
       "          7.57485294]], dtype=float32)"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "thresholds = threshold_spacing @ (response_max - response_min) + response_min\n",
    "thresholds.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6. Use the thresholds to divide the responses into two groups\n",
    "\n",
    "To divide the responses based on these thresholds, we can simply compare the responses to the thresholds. In order for this to work out, the tensor dimensions need to agree. We can do this by tiling the thresholds and per-sample responses. The result of this is a rank 3 tensor, `n_surfaces` $\\times$ `n_thresholds` $\\times$ `n` for each of the inputs, where the per-surface thresholds have been duplicated for each sample and the per-surface responses have been duplicated for each threshold. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[[  1.06634331,   1.06634331,   1.06634331,   1.06634331],\n",
       "        [  4.56634331,   4.56634331,   4.56634331,   4.56634331],\n",
       "        [  8.06634331,   8.06634331,   8.06634331,   8.06634331]],\n",
       "\n",
       "       [[  6.9365406 ,   6.9365406 ,   6.9365406 ,   6.9365406 ],\n",
       "        [  9.1865406 ,   9.1865406 ,   9.1865406 ,   9.1865406 ],\n",
       "        [ 11.4365406 ,  11.4365406 ,  11.4365406 ,  11.4365406 ]],\n",
       "\n",
       "       [[ -0.03203011,  -0.03203011,  -0.03203011,  -0.03203011],\n",
       "        [  3.46796989,   3.46796989,   3.46796989,   3.46796989],\n",
       "        [  6.96796989,   6.96796989,   6.96796989,   6.96796989]],\n",
       "\n",
       "       [[  5.16289711,   5.16289711,   5.16289711,   5.16289711],\n",
       "        [  8.66289711,   8.66289711,   8.66289711,   8.66289711],\n",
       "        [ 12.16289711,  12.16289711,  12.16289711,  12.16289711]],\n",
       "\n",
       "       [[  1.07485294,   1.07485294,   1.07485294,   1.07485294],\n",
       "        [  4.32485294,   4.32485294,   4.32485294,   4.32485294],\n",
       "        [  7.57485294,   7.57485294,   7.57485294,   7.57485294]]], dtype=float32)"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sample_thresholds = tf.tile(tf.reshape(tf.transpose(thresholds), [n_surfaces, n_thresholds, 1]), [1,1, n]) \n",
    "sample_thresholds.eval()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[[  5.56634331,   5.6865406 ,   4.46796989,   9.66289711],\n",
       "        [  5.56634331,   5.6865406 ,   4.46796989,   9.66289711],\n",
       "        [  5.56634331,   5.6865406 ,   4.46796989,   9.66289711]],\n",
       "\n",
       "       [[ -1.17514706,  -1.43365669,  10.6865406 ,  -2.53203011],\n",
       "        [ -1.17514706,  -1.43365669,  10.6865406 ,  -2.53203011],\n",
       "        [ -1.17514706,  -1.43365669,  10.6865406 ,  -2.53203011]],\n",
       "\n",
       "       [[  2.66289711,   7.82485294,  11.56634331,  13.6865406 ],\n",
       "        [  2.66289711,   7.82485294,  11.56634331,  13.6865406 ],\n",
       "        [  2.66289711,   7.82485294,  11.56634331,  13.6865406 ]],\n",
       "\n",
       "       [[ 10.46796989,  15.66289711,  10.82485294,  -2.43365669],\n",
       "        [ 10.46796989,  15.66289711,  10.82485294,  -2.43365669],\n",
       "        [ 10.46796989,  15.66289711,  10.82485294,  -2.43365669]],\n",
       "\n",
       "       [[  4.6865406 ,  -3.53203011,   1.66289711,  -2.17514706],\n",
       "        [  4.6865406 ,  -3.53203011,   1.66289711,  -2.17514706],\n",
       "        [  4.6865406 ,  -3.53203011,   1.66289711,  -2.17514706]]], dtype=float32)"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "response_tiles = tf.tile(tf.reshape(responses, [-1, 1, n]), [1, n_thresholds, 1])\n",
    "response_tiles.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we can just compare. Note in the display result below, there appear to be 5 matrices, one for each surface, each with one row per threshold and one column per sample. The value at each location is 0 if the response of that sample to that surface was less than that threshold, 1 if it was greater. Also note that although the display is formatted to indicate separate matrices, it is actually as single 5x3x4 tensor which is the key to its efficient execution on a GPU."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[[1, 1, 1, 1],\n",
       "        [1, 1, 0, 1],\n",
       "        [0, 0, 0, 1]],\n",
       "\n",
       "       [[0, 0, 1, 0],\n",
       "        [0, 0, 1, 0],\n",
       "        [0, 0, 0, 0]],\n",
       "\n",
       "       [[1, 1, 1, 1],\n",
       "        [0, 1, 1, 1],\n",
       "        [0, 1, 1, 1]],\n",
       "\n",
       "       [[1, 1, 1, 0],\n",
       "        [1, 1, 1, 0],\n",
       "        [0, 1, 0, 0]],\n",
       "\n",
       "       [[1, 0, 1, 0],\n",
       "        [1, 0, 0, 0],\n",
       "        [0, 0, 0, 0]]], dtype=int32)"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "splits = tf.cast(response_tiles > sample_thresholds, tf.int32)\n",
    "splits.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 7. Entropy\n",
    "We now have 5 surfaces and 3 thresholds for each surface that we have used to propose splits of the input data. Our task is to choose the best split. We define the best split as the split that leads to the highest information gain, where information gain $I$ is defined as\n",
    "$$ I(S,S_L,S_R) = H(S) - \\sum_{i \\in \\{L,R\\}}\\frac{|S_i|}{|S|}H(S_i) $$\n",
    "Where $S$ is the sample set and $S_L, S_R$ are the proposed left and right spits. $H(S)$ is the shannon entropy of the set with respect to the classes defined as \n",
    "$$H(S) = -\\sum_{c \\in C}p(c)\\log_2(p(c))$$\n",
    "We define $p(c)$ as a simple normalized histogram over the labels, e.g. we count the number of each class and divide by the total. Because we have used one-hot labels, this ammounts to a sum over the label set on axis 0 (rows)\n",
    "We implement now implement these two equations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "    def entropy(y):\n",
    "        p = tf.reduce_sum(y, axis=0) / tf.cast(tf.shape(y)[0],  dtype=tf.float32)\n",
    "        informations = p * tf.log(p) / tf.log(2.)\n",
    "        informations = tf.where(tf.is_inf(informations), tf.zeros_like(informations), informations)\n",
    "        e = -tf.reduce_sum(informations)\n",
    "        return tf.where(tf.is_nan(e), tf.zeros_like(e), e) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "    def information_gain(full_set_entropy, left_set, right_set):\n",
    "        m_l = tf.shape(left_set)[0]\n",
    "        m_r = tf.shape(right_set)[0]\n",
    "        m_t = m_l + m_r\n",
    "        \n",
    "        left_entropy_ratio =tf.cast(m_l / m_t, tf.float32) * entropy(left_set)\n",
    "        right_entropy_ratio = tf.cast(m_r / m_t, tf.float32) * entropy(right_set)\n",
    "\n",
    "        return full_set_entropy - (left_entropy_ratio + right_entropy_ratio)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and we compute the entropy of our full set, observe that since both labels `[0, 1]` and `[1, 0]` are equally likely, the entropy of our current label set is 1.0 bits (the highest possible entropy)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.0"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "full_set_entropy = entropy(labels)\n",
    "full_set_entropy.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 8. Compute the Information Gain of Each Split\n",
    "We can now compute the information gain of each proposed split. Note that there is no way to do this using only tensors as each surface/threshold combo potentially has a different number of samples assigned to it and there for a different 3rd dimension. Instead, we break up the splits by surface and threshold, giving us a nested lists of 4-vectors, the split direction for each of the 4 samples. Then we can reduce that 4 vector to a scalar, the information gained by the split using the functions in section 7. Finally, we stack the information gain values into a single rank 2 tesnor, $5 \\times 3$, where the value at position $i,j$ is the information gained by splitting the samples using surface $i$ and threshold $j$. Beacuse we can't use tensors for this, it is extremely slow."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 0.        ,  0.31127816,  0.31127816],\n",
       "       [ 0.31127816,  0.31127816,  0.        ],\n",
       "       [ 0.        ,  0.31127816,  0.31127816],\n",
       "       [ 0.31127816,  0.31127816,  0.31127816],\n",
       "       [ 0.        ,  0.31127816,  0.        ]], dtype=float32)"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "per_surface = tf.split(splits,  num_or_size_splits=n_surfaces, axis=0)\n",
    "per_threshold = [tf.split(p_s, num_or_size_splits=n_thresholds, axis=1) for p_s in per_surface]\n",
    "left_and_right_sets = [[tf.dynamic_partition(labels,  partitions=tf.reshape(p_s_t, [n]), num_partitions=2) for p_s_t in p_s] for p_s in per_threshold]\n",
    "information_gained_by_splits = [[information_gain(full_set_entropy, s_l, s_r) for s_l, s_r in p_s] for p_s in left_and_right_sets]\n",
    "tensor_ig = tf.stack([tf.stack(p_s) for p_s in information_gained_by_splits], axis=0)\n",
    "tensor_ig.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 9. Recover the Best Surface and Threshold\n",
    "With the information gains computed, we can recover the best surface and threshold by discrete optimization over the set of information gains. We pick the maximum element from the matrix and use its indices to store the best surface and best threshold."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([ 0.        ,  1.        ,  0.        ,  7.73872852], dtype=float32)"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "best_surface_ind = tf.argmax(tf.reduce_max(tensor_ig, axis=0))\n",
    "best_surface = surfaces[best_surface_ind,:]\n",
    "best_surface.eval()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "6.9365406"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "best_threshold_ind = tf.argmax(tensor_ig[best_surface_ind, :], axis=0)\n",
    "best_threshold = thresholds[best_surface_ind,best_threshold_ind]\n",
    "best_threshold.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 10. Divide the samples using the best surface and threshold\n",
    "Finally we can use the best sample and threshold to divide the input set into its left and right splits"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([1, 1, 0, 1], dtype=int32)"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "best_split = splits[best_surface_ind, best_threshold_ind]\n",
    "best_split.eval()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "samples_r, samples_l = tf.dynamic_partition(x, best_split, num_partitions=2)\n",
    "labels_r, labels_l = tf.dynamic_partition(labels, best_split, num_partitions=2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(array([[ 7.,  8.,  9.]], dtype=float32), array([[ 0.,  1.]], dtype=float32))"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "samples_r.eval(), labels_r.eval()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(array([[-1.,  2., -3.],\n",
       "        [ 4., -5.,  6.],\n",
       "        [-2., -6., -4.]], dtype=float32), array([[ 1.,  0.],\n",
       "        [ 1.,  0.],\n",
       "        [ 0.,  1.]], dtype=float32))"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "samples_l.eval(), labels_l.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These samples can then be routed to the children of the current node, and the process repeated either until a certain tree depth is reached or all of information gains are less than some $\\epsilon$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Appendix A: What to Do at a Leaf Node\n",
    "\n",
    "Lets assume that the children of the node we computed in the previous section are leaf nodes, how can we make a decision based on the given data? In formal terms, we want to measure the posterior probability $p(y|x)$ for some $x$ arriving at our node. Since the purpose of the decision tree is to reduce the entropy in deeper nodes, we can assume that we have low entropy and use a histogram as we did when computing the entropy $H(S)$. We compute these distributions for our left and right children."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([ 0.,  1.], dtype=float32)"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "p_y_r =  tf.reduce_sum(labels_r, axis=0) / tf.cast(tf.shape(labels_r)[0], tf.float32)\n",
    "p_y_r.eval()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([ 0.66666669,  0.33333334], dtype=float32)"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "p_y_l =  tf.reduce_sum(labels_l, axis=0) / tf.cast(tf.shape(labels_l)[0], tf.float32)\n",
    "p_y_l.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This indicates that samples arriving at the left child have a 66% change of being labele `[1, 0]` and a 33% chance of being label `[0, 1]`. If asked to make a hard decision (e.g. inference), we would pick label `[1, 0]` for all samples arriving at the right child. Samples arriving at the right child, however, have a 100% chance of being label `[0, 1]`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Appendix B: Inference\n",
    "\n",
    "Now assume we have a test sample, how do we predict its class using this decision tree? First lets make a testing example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 0., -1.,  2.]], dtype=float32)"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "test_example = tf.constant([[0., -1., 2.]])\n",
    "test_example.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and we augment it as we did for training samples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 0., -1.,  2.,  1.]], dtype=float32)"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "test_example_aug = tf.concat([test_example, [[1]]], axis=1)\n",
    "test_example_aug.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then compute its response against the best surface that was selected during training, and then use the best threshold to determine which child to send it to"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 5.24566936]], dtype=float32)"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "response = test_example_aug @ tf.transpose(tf.reshape(best_surface, [-1, 4]))\n",
    "response.eval()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ True]], dtype=bool)"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "split = response > best_threshold\n",
    "split.eval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The response is greater, so we send it to the right child. As computed in Appendix A, this means it has a 100% chance (given the training data) of being class `[0, 1]`."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
