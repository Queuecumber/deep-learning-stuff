from RBM.RBM import RBM

import numpy as np
import tensorflow as tf
from tensorflow.contrib.learn.python.learn.estimators import model_fn as model_fn_lib
from tensorflow.contrib.learn.python.learn.estimators import estimator


class DRBM(RBM):
    def __init__(self,
                 n_visible,
                 n_hidden,
                 n_labels,
                 visible_type='binary',
                 loss_type='generative',
                 W=None,
                 U=None,
                 b_v=None,
                 b_h=None,
                 b_l=None
                 ):

        super(DRBM, self).__init__(
            n_visible,
            n_hidden,
            visible_type,
            W, b_v, b_h
        )

        if U is None:
            U = tf.Variable(tf.truncated_normal([n_hidden, n_labels]), name='U')
        else:
            U = tf.Variable(U)

        if b_l is None:
            b_l = tf.Variable(tf.zeros((1, n_labels)), name='b_l')
        else:
            b_l = tf.Variable(b_l)

        self.n_labels = n_labels
        self.U = U
        self.b_l = b_l
        self.loss_type = loss_type

    def activate_hidden(self, s_v, s_l):
        return tf.nn.sigmoid(s_v @ self.W + s_l @ tf.transpose(self.U) + self.b_h, name='hidden_activation')

    def h_given_v_l(self, s_v, s_l):
        E_h = self.activate_hidden(s_v, s_l)
        s_h = tf.contrib.distributions.Bernoulli(probs=E_h, dtype=tf.float32).sample()
        return s_h, E_h

    def activate_label(self, s_h):
        return tf.nn.softmax(s_h @ self.U + self.b_l)

    def l_given_h(self, s_h):
        E_l = self.activate_label(s_h)
        s_l = tf.one_hot(tf.contrib.distributions.Categorical(probs=E_l).sample(), depth=self.n_labels)
        return s_l, E_l

    def l_given_v(self, s_v):
        visible_projection = tf.reshape(s_v @ self.W, [-1, 1, self.n_hidden])
        interaction_term = tf.nn.softplus(self.b_h + tf.transpose(self.U) + visible_projection)
        marginalize_hidden = tf.reduce_sum(interaction_term, axis=2) + self.b_l
        unlog = tf.exp(marginalize_hidden - tf.reduce_max(marginalize_hidden, axis=1, keep_dims=True))
        normalizer = tf.reduce_sum(unlog, axis=1, keep_dims=True)

        E_l = unlog / normalizer
        s_l = tf.one_hot(tf.contrib.distributions.Categorical(probs=E_l).sample(), depth=self.n_labels)
        return s_l, E_l

    def gibbs_hvlh(self, s_h0):
        s_v1, E_v1 = self.v_given_h(s_h0)
        s_l1, E_l1 = self.l_given_h(s_h0)
        s_h1, E_h1 = self.h_given_v_l(s_v1, s_l1)
        return s_v1, E_v1, s_l1, E_l1, s_h1, E_h1

    def gibbs_vlhvl(self, s_v0, s_l0):
        s_h1, E_h1 = self.h_given_v_l(s_v0, s_l0)
        s_v1, E_v1 = self.v_given_h(s_h1)
        s_l1, E_l1 = self.l_given_h(s_h1)
        return s_h1, E_h1, s_v1, E_v1, s_l1, E_l1

    def free_energy(self, s_v, s_l):
        with tf.name_scope('free_energy'):
            interaction_term = s_v @ self.W + s_l @ tf.transpose(self.U) + self.b_h
            hidden_term = tf.reduce_sum(
                tf.nn.softplus(interaction_term),
                axis=1,
                keep_dims=True
            )

            if self.visible_type == 'binary':
                vbias_term = -(s_v @ tf.transpose(self.b_v))
            elif self.visible_type == 'real':
                vbias_term = tf.reduce_sum((s_v - self.b_v)**2 / 2)

            lbias_term = s_l @ tf.transpose(self.b_l)

            return vbias_term - lbias_term - hidden_term

    def discriminative_loss(self, x, y):
        _, proposed_label_probs = self.l_given_v(x)
        proposed_log_likelihoods = proposed_label_probs
        proposed_label_log_likelihoods = tf.reduce_sum(proposed_log_likelihoods * y, axis=1)
        batch_loss = -tf.reduce_mean(proposed_label_log_likelihoods)
        return batch_loss

    def constrastive_divergence_loss(self, x, y, k=15, persistent_state=None):
        with tf.name_scope('constrastive_divergence_loss'):
            if persistent_state is not None:
                s_h0, E_h0 = (persistent_state, None)
            else:
                s_h0, E_h0 = self.h_given_v_l(x, y)

            s_vk, E_vk, s_lk, E_lk, s_hk, E_hk = (None, None, None, None, s_h0, E_h0)
            for _ in range(k):
                s_vk, E_vk, s_lk, E_lk, s_hk, E_hk = self.gibbs_hvlh(s_hk)

            if persistent_state is not None:
                persistent_state.assign(s_hk)

            s_vk = tf.stop_gradient(s_vk)
            s_lk = tf.stop_gradient(s_lk)
            return tf.reduce_mean(self.free_energy(x, y)) - tf.reduce_mean(self.free_energy(s_vk, s_lk))

    def hybrid_loss(self, x, y, alpha, k=15, persistent_state=None):
        return self.discriminative_loss(x, y) - alpha * self.constrastive_divergence_loss(x, y, k, persistent_state)

    def fit(self, x, y, learning_rate, optimizer=None, alpha=0.5, k=15, persistent_state=None, global_step=None):
        if optimizer is None:
            optimizer = tf.train.GradientDescentOptimizer(learning_rate)

        if self.loss_type == 'generative':
            loss = self.constrastive_divergence_loss(x, y, k, persistent_state)
        elif self.loss_type == 'discriminative':
            loss = self.discriminative_loss(x, y)
        elif self.loss_type == 'hybrid':
            loss = self.hybrid_loss(x, y, alpha)

        train_step = optimizer.minimize(loss, global_step=global_step, var_list=[self.W, self.U, self.b_v, self.b_h, self.b_l])
        return train_step, loss

    def encode(self, x, y=None):
        if y is None:
            y = self.predict(x)

        return self.h_given_v_l(x, y)

    def predict(self, x, chain_length=1000):

        if self.loss_type == 'generative':
            label_shape = [tf.shape(x)[0], self.n_labels]

            equal_prob = tf.reshape(tf.tile([1. / self.n_labels], tf.reduce_prod(label_shape, keep_dims=True)), label_shape)
            s_lk = tf.one_hot(tf.contrib.distributions.Categorical(probs=equal_prob).sample(), depth=self.n_labels)

            for _ in range(chain_length):
                s_hk, E_hk = self.h_given_v_l(x, s_lk)
                s_lk, E_lk = self.l_given_h(s_hk)
        elif self.loss_type == 'discriminative' or self.loss_type == 'hybrid':
            s_lk, _ = self.l_given_v(x)

        return s_lk

    def generate(self, y, chain_length=1000):
        visible_shape = [tf.shape(y)[0], self.n_visible]

        if self.visible_type == 'real':
            s_vk = tf.truncated_normal(visible_shape)
        elif self.visible_type == 'binary':
            equal_prob = tf.reshape(tf.tile([0.5], tf.reduce_prod(visible_shape, keep_dims=True)), visible_shape)
            s_vk = tf.contrib.distributions.Bernoulli(probs=equal_prob, dtype=tf.float32).sample()

        for _ in range(chain_length):
            s_hk, E_hk = self.h_given_v_l(s_vk, y)
            s_vk, E_vk = self.v_given_h(s_hk)

        return s_vk


def _drbm_model_fn(features, labels, mode, params):
    n_visible = params.get('n_visible')
    n_hidden = params.get('n_hidden')
    n_labels = params.get('n_labels')
    W = params.get('W')
    U = params.get('U')
    b_v = params.get('b_v')
    b_h = params.get('b_h')
    b_l = params.get('b_l')
    visible_type = params.get('visible_type') or 'binary'
    loss_type = params.get('loss_type') or 'generative'

    drbm_model = DRBM(n_visible, n_hidden, n_labels, visible_type, loss_type, W, U, b_v, b_h, b_l)

    lr = params.get('learning_rate')
    optimizer = params.get('optimizer')
    k = params.get('k', 15)
    chain_length = params.get('chain_length', 1000)

    if params.get('persistent_state') is not None:
        persistent_state = tf.Variable(params.get('persistent_state'), dtype=tf.float32)
    else:
        persistent_state = None

    prediction = {}
    if mode == model_fn_lib.ModeKeys.INFER:
        if features.get_shape()[1] == n_visible:
            prediction[DRBMEstimator.PREDICT] = drbm_model.predict(features, chain_length)
            prediction[DRBMEstimator.ENCODE] = drbm_model.encode(features, labels)

        if features.get_shape()[0] == n_labels:
            prediction[DRBMEstimator.GENERATE] = drbm_model.generate(features, chain_length)

    eval_metric_ops = {}
    if mode == model_fn_lib.ModeKeys.EVAL:
        eval_metric_ops = {
            'accuracy': tf.metrics.accuracy(labels, drbm_model.predict(features, chain_length))
        }

    train_step, loss = None, None
    if mode == model_fn_lib.ModeKeys.TRAIN or mode == model_fn_lib.ModeKeys.EVAL:
        train_step, loss = drbm_model.fit(features, labels, lr, optimizer, k, persistent_state, tf.contrib.framework.get_global_step())

    return model_fn_lib.ModelFnOps(
        mode=mode,
        loss=loss,
        train_op=train_step,
        predictions=prediction,
        eval_metric_ops=eval_metric_ops
    )


class DRBMEstimator(estimator.Estimator):
    ENCODE = 'encode'
    GENERATE = 'generate'
    PREDICT = 'predict'

    def __init__(self,
                 n_visible,
                 n_hidden,
                 n_labels,
                 learning_rate,
                 visible_type='binary',
                 loss_type='generative',
                 persistent_state=None,
                 W=None,
                 U=None,
                 b_v=None,
                 b_h=None,
                 b_l=None,
                 k=15,
                 chain_length=1000,
                 optimizer=None,
                 model_dir=None,
                 config=None):

        params = {
            'n_visible': n_visible,
            'n_hidden': n_hidden,
            'n_labels': n_labels,
            'visible_type': visible_type,
            'loss_type': loss_type,
            'W': W,
            'U': U,
            'b_v': b_v,
            'b_h': b_h,
            'b_l': b_l,
            'k': k,
            'optimizer': optimizer,
            'learning_rate': learning_rate,
            'chain_length': chain_length,
            'persistent_state': persistent_state
        }

        model_fn = _drbm_model_fn

        super(DRBMEstimator, self).__init__(
            model_fn=model_fn,
            model_dir=model_dir,
            config=config,
            params=params
        )

    def predict(self, input_fn=None):
        key = DRBMEstimator.PREDICT
        results = super(DRBMEstimator, self).predict(
            input_fn=input_fn, outputs=[key]
        )

        for result in results:
            yield result[key]

    def encode(self, input_fn=None):
        key = DRBMEstimator.ENCODE
        results = super(DRBMEstimator, self).predict(
            input_fn=input_fn, outputs=[key]
        )

        for result in results:
            yield result[key]

    def generate(self, input_fn=None):
        key = DRBMEstimator.GENERATE
        results = super(DRBMEstimator, self).predict(
            input_fn=input_fn, outputs=[key]
        )

        for result in results:
            yield result[key]
