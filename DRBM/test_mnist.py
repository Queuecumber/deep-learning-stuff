from DRBM import DRBM, DRBMEstimator
import tensorflow as tf
import numpy as np
from tensorflow.examples.tutorials.mnist import input_data

from tensorflow.python import debug as tf_debug

mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

tf.logging.set_verbosity(tf.logging.INFO)


def mnist_input_fn(dataset, batch_size, epochs):
    def input_fn():
        batch = dataset.next_batch(batch_size)
        return tf.constant(batch[0], dtype=tf.float32), tf.constant(batch[1], dtype=tf.float32)

    n_steps = (dataset.num_examples // batch_size) * epochs

    return input_fn, n_steps


def zscore_dataset(dataset, mean=None, dev=None):
    if mean is None:
        mean = np.mean(dataset._images, axis=0)

    if dev is None:
        dev = np.std(dataset._images, axis=0)
        dev[dev == 0] = 1.

    dataset._images = (dataset.images - mean) / dev

    return mean, dev


def test_estimator():
    batch_size = 64
    epochs = 100

    mean, dev = zscore_dataset(mnist.train)
    zscore_dataset(mnist.test, mean, dev)

    train_input_fn, n_steps = mnist_input_fn(mnist.train, batch_size=batch_size, epochs=epochs)
    test_input_fn, test_steps = mnist_input_fn(mnist.test, batch_size=1000, epochs=1)

    rbm = DRBMEstimator(n_visible=28 * 28,
                        n_hidden=500,
                        n_labels=10,
                        learning_rate=0.001,
                        visible_type='real',
                        loss_type='discriminative',
                        persistent_state=np.zeros((batch_size, 500)),
                        model_dir='mnist_model')

    hooks = tf_debug.LocalCLIDebugHook()
    hooks.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)

    print('Running for {} steps with batch size {} ({} epochs)'.format(n_steps, batch_size, epochs))
    rbm.fit(input_fn=train_input_fn, max_steps=n_steps, monitors=[hooks])

    ev = rbm.evaluate(input_fn=test_input_fn, steps=test_steps)

    generated = list(rbm.generate(input_fn=lambda: tf.eye(10)))

    generated = generated * dev + mean

    generated_images = tf.reshape(tf.constant(np.vstack(generated)), [10, 28, 28, 1])
    summary_generated = tf.summary.image('generated-samples', generated_images, max_outputs=10)

    with tf.Session() as sess:
        test_writer = tf.summary.FileWriter('mnist_model/generated', sess.graph)
        s = sess.run(summary_generated)
        test_writer.add_summary(s)
        test_writer.close()


def test_graphbuilder():
    batch_size = 64
    epochs = 100

    #mean, dev = zscore_dataset(mnist.train)
    #zscore_dataset(mnist.test, mean, dev)

    rbm = DRBM(n_visible=28 * 28,
               n_hidden=500,
               n_labels=10,
               visible_type='binary',
               loss_type='discriminative')

    x = tf.placeholder(dtype=tf.float32, shape=[None, 28 * 28], name='x')
    y_ = tf.placeholder(dtype=tf.float32, shape=[None, 10], name='y_')

    train_step, loss = rbm.fit(x=x, y=y_, learning_rate=0.05)

    y = rbm.predict(x=x)
    correct = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))

    generated = rbm.generate(tf.eye(10)) # * dev + mean
    generated_image_set = tf.reshape(generated, [10, 28, 28, 1])
    summary_generated = tf.summary.image('generated-samples', generated_image_set, max_outputs=10)

    print('Running with batch size {} ({} epochs)'.format(batch_size, epochs))

    n_batches = mnist.train.num_examples // batch_size
    with tf.Session() as sess:

        # sess = tf_debug.LocalCLIDebugWrapperSession(sess)
        # sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)

        sess.run(tf.global_variables_initializer())
        for e in range(epochs):
            batch_losses = []
            for i in range(n_batches):
                batch = mnist.train.next_batch(batch_size)
                [_, l] = sess.run([train_step, loss], feed_dict={x: batch[0], y_: batch[1]})
                batch_losses.append(l)

            print('Epoch {} average loss {}'.format(e, np.mean(batch_losses)))

        a = sess.run([accuracy], feed_dict={x: mnist.test.images, y_: mnist.test.labels})
        print('Testing accuracy {}'.format(a))

        print('Generating samples')
        test_writer = tf.summary.FileWriter('mnist_model/generated', sess.graph)
        s = sess.run(summary_generated)
        test_writer.add_summary(s)
        test_writer.close()


if __name__ == '__main__':
    test_graphbuilder()
