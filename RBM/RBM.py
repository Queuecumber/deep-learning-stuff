import numpy as np
import tensorflow as tf
from tensorflow.contrib.learn.python.learn.estimators import model_fn as model_fn_lib
from tensorflow.contrib.learn.python.learn.estimators import estimator


class RBM(object):
    def __init__(self,
                 n_visible,
                 n_hidden,
                 visible_type='binary',
                 W=None,
                 b_v=None,
                 b_h=None
                 ):

        self.weight_initializer = tf.contrib.layers.xavier_initializer(uniform=False)
        self.bias_initializer = tf.contrib.layers.xavier_initializer()

        if W is None:
            W = tf.Variable(self.weight_initializer([n_visible, n_hidden]), name='W')
        else:
            W = tf.Variable(W)

        if b_v is None:
            b_v = tf.Variable(self.bias_initializer([1, n_visible]), name='b_v')
        else:
            b_v = tf.Variable(b_v)

        if b_h is None:
            b_h = tf.Variable(self.bias_initializer([1, n_hidden]), name='b_h')
        else:
            b_h = tf.Variable(b_h)

        self.visible_type = visible_type
        self.n_visible = n_visible
        self.n_hidden = n_hidden
        self.W = W
        self.b_v = b_v
        self.b_h = b_h
        self.persistent_cd_state = None
        #self.sigma = tf.Variable(self.bias_initializer([1, n_visible]))

    def activate_hidden(self, s_v):
        if self.visible_type == 'binary':
            return tf.nn.sigmoid(s_v @ self.W + self.b_h, name='hidden_activation')
        elif self.visible_type == 'real':
            return tf.nn.relu(s_v @ self.W + self.b_h)

    def h_given_v(self, s_v):
        E_h = self.activate_hidden(s_v)

        if self.visible_type == 'binary':
            s_h = tf.contrib.distributions.Bernoulli(probs=E_h, dtype=tf.float32).sample()
        elif self.visible_type == 'real':
            s_h = tf.cast(E_h > 0, tf.float32)
        return s_h, E_h

    def activate_visible(self, s_h):
        if self.visible_type == 'binary':
            return tf.nn.sigmoid(s_h @ tf.transpose(self.W) + self.b_v, name='visible_activation')
        elif self.visible_type == 'real':
            return s_h @ tf.transpose(self.W) + self.b_v

    def v_given_h(self, s_h):
        E_v = self.activate_visible(s_h)

        if self.visible_type == 'binary':
            s_v = tf.contrib.distributions.Bernoulli(probs=E_v, dtype=tf.float32).sample()
            #s_v = tf.round(E_v)
        elif self.visible_type == 'real':
            s_v = tf.truncated_normal(tf.shape(E_v), E_v)
            #s_v = E_v

        return s_v, E_v

    def gibbs_hvh(self, s_h0):
        s_v1, E_v1 = self.v_given_h(s_h0)
        s_h1, E_h1 = self.h_given_v(s_v1)
        return s_v1, E_v1, s_h1, E_h1

    def gibbs_vhv(self, s_v0):
        s_h1, E_h1 = self.h_given_v(s_v0)
        s_v1, E_v1 = self.v_given_h(s_h1)
        return s_h1, E_h1, s_v1, E_v1

    def free_energy(self, s_v):
        with tf.name_scope('free_energy'):
            if self.visible_type == 'binary':
                interaction_term = s_v  @ self.W + self.b_h
            elif self.visible_type == 'real':
                interaction_term = s_v @ self.W + self.b_h

            hidden_term = tf.reduce_sum(
                tf.nn.relu(interaction_term),
                axis=1,
                keep_dims=True
            )

            if self.visible_type == 'binary':
                vbias_term = -(s_v @ tf.transpose(self.b_v))
            elif self.visible_type == 'real':
                vbias_term = tf.reduce_sum((s_v - self.b_v)**2 / (2.))

            return vbias_term - hidden_term

    def constrastive_divergence_loss(self, x, chain_length=1, persistent=False):
        with tf.name_scope('constrastive_divergence_loss'):
            if persistent:
                if self.persistent_cd_state is None:
                    self.persistent_cd_state = tf.Variable(tf.zeros([tf.shape(x)[0], self.n_hidden]), validate_shape=False)

                s_h0, E_h0 = (self.persistent_cd_state, None)
            else:
                s_h0, E_h0 = self.h_given_v(x)

            s_vk, E_vk, s_hk, E_hk = (None, None, s_h0, E_h0)
            for _ in range(chain_length):
                s_vk, E_vk, s_hk, E_hk = self.gibbs_hvh(s_hk)

            if persistent:
                self.persistent_cd_state.assign(s_hk)

            s_vk = tf.stop_gradient(s_vk)
            return tf.reduce_mean(self.free_energy(x)) - tf.reduce_mean(self.free_energy(s_vk))

    def fit(self, x, learning_rate, optimizer=None, chain_length=1, persistent=False, global_step=None):
        if optimizer is None:
            optimizer = tf.train.GradientDescentOptimizer(learning_rate)

        loss = self.constrastive_divergence_loss(x, chain_length, persistent)
        train_step = optimizer.minimize(loss, global_step=global_step, var_list=[self.W, self.b_v, self.b_h])
        return train_step, loss

    def encode(self, x):
        return self.h_given_v(x)

    def generate(self, x, chain_length=1000):
        s_vk = x
        for _ in range(chain_length):
            s_hk, E_hk, s_vk, E_vk = self.gibbs_vhv(s_vk)
        return s_vk


def _rbm_model_fn(features, labels, mode, params):
    n_visible = params.get('n_visible')
    n_hidden = params.get('n_hidden')
    W = params.get('W')
    b_v = params.get('b_v')
    b_h = params.get('b_h')
    visible_type = params.get('visible_type', 'binary')

    rbm_model = RBM(n_visible, n_hidden, visible_type, W, b_v, b_h)

    lr = params.get('learning_rate')
    optimizer = params.get('optimizer')
    chain_length_fitting = params.get('chain_length_fitting', 1)
    chain_length_generating = params.get('chain_length_generating', 1000)

    persistent_cd = params.get('persistent', False)

    predictions = {
        RBMEstimator.ENCODE: rbm_model.encode(features),
        RBMEstimator.GENERATE: rbm_model.generate(features, chain_length_generating)
    }

    train_step, loss = rbm_model.fit(features,
                                     lr,
                                     optimizer,
                                     chain_length_fitting,
                                     persistent_cd,
                                     tf.contrib.framework.get_global_step())

    return model_fn_lib.ModelFnOps(
        mode=mode,
        loss=loss,
        train_op=train_step,
        predictions=predictions
    )


class RBMEstimator(estimator.Estimator):
    ENCODE = 'encode'
    GENERATE = 'generate'

    def __init__(self,
                 learning_rate,
                 n_visible,
                 n_hidden=500,
                 visible_type='binary',
                 persistent=False,
                 W=None,
                 b_v=None,
                 b_h=None,
                 chain_length_fitting=1,
                 chain_length_generating=1000,
                 optimizer=None,
                 model_dir=None,
                 config=None):

        params = {
            'n_visible': n_visible,
            'n_hidden': n_hidden,
            'visible_type': visible_type,
            'W': W,
            'b_v': b_v,
            'b_h': b_h,
            'chain_length_fitting': chain_length_fitting,
            'optimizer': optimizer,
            'learning_rate': learning_rate,
            'chain_length_generating': chain_length_generating,
            'persistent': persistent
        }

        model_fn = _rbm_model_fn

        super(RBMEstimator, self).__init__(
            model_fn=model_fn,
            model_dir=model_dir,
            config=config,
            params=params
        )

    def encode(self, input_fn=None):
        key = RBMEstimator.ENCODE
        results = super(RBMEstimator, self).predict(
            input_fn=input_fn, outputs=[key]
        )

        for result in results:
            yield result[key]

    def generate(self, input_fn=None):
        key = RBMEstimator.GENERATE
        results = super(RBMEstimator, self).predict(
            input_fn=input_fn, outputs=[key]
        )

        for result in results:
            yield result[key]
