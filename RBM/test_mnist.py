from RBM import RBMEstimator
import tensorflow as tf
import numpy as np
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

tf.logging.set_verbosity(tf.logging.INFO)


def mnist_input_fn(dataset, batch_size, epochs):
    def input_fn():
        batch = dataset.next_batch(batch_size)
        return tf.constant(batch[0]), tf.constant(batch[1])

    n_steps = (dataset.num_examples // batch_size) * epochs

    return input_fn, n_steps


def zscore_dataset(dataset, mean=None, dev=None):

    if mean is None:
        mean = np.mean(dataset._images, axis=0)

    if dev is None:
        dev = np.var(dataset._images, axis=0)
        dev[dev == 0] = 1.

    dataset._images = (dataset.images - mean) / dev

    return mean, dev


if __name__ == '__main__':
    batch_size = 20
    epochs = 20

    mean, dev = zscore_dataset(mnist.train)
    zscore_dataset(mnist.test, mean, dev)

    rbm = RBMEstimator(learning_rate=0.001,
                       n_visible=28 * 28,
                       visible_type='real',
                       n_hidden=500,
                       persistent=False,
                       chain_length_fitting=15,
                       model_dir='summaries')

    train_input_fn, n_steps = mnist_input_fn(mnist.train, batch_size=batch_size, epochs=epochs)
    print('Running for {} steps with batch size {} ({} epochs)'.format(n_steps, batch_size, epochs))
    rbm.fit(input_fn=train_input_fn, max_steps=n_steps)

    image_summaries = []

    samples = mnist.test.next_batch(10)[0]
    generated = list(rbm.generate(input_fn=lambda: samples))

    samples = samples * dev + mean
    generated = generated * dev + mean

    sample_images = tf.reshape(tf.constant(samples), [10, 28, 28, 1])
    generated_images = tf.reshape(tf.constant(np.vstack(generated)), [10, 28, 28, 1])

    summary_images = tf.summary.image('selected-samples', sample_images, max_outputs=10)
    summary_generated = tf.summary.image('generated-samples', generated_images, max_outputs=10)

    with tf.Session() as sess:
        test_writer = tf.summary.FileWriter('summaries/test', sess.graph)
        s, g = sess.run([summary_images, summary_generated])
        test_writer.add_summary(s)
        test_writer.add_summary(g)
        test_writer.close()
